sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/m/MessageBox"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, MessageBox) {
        "use strict";

        return Controller.extend("zccsuifinancialdata.controller.View2", {
            onInit: function () {
                this._onInitJson();
                var oRouter = this.getOwnerComponent().getRouter();
                oRouter.getRoute("RouteView2").attachPatternMatched(this._onObjectMatched, this);
            },
            _onObjectMatched: function (oEvent) {
                var sObjectId = oEvent.getParameter("arguments").objectId;
                this._getFinData(sObjectId);

            },
            _getFinData: function (sObjectId) {
                var sfilterh = "?$filter=(application_no eq '" + sObjectId + "')";
                var aFinancialDataBHeaders = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/FinancialDataService/FinancialDataBHeaders") + sfilterh,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aFinancialDataBHeaders = data.value; }
                });

                var oModel = this.getView().getModel();
                oModel.setProperty("/view/application_no", aFinancialDataBHeaders[0].application_no);
                oModel.setProperty("/view/create_date", aFinancialDataBHeaders[0].create_date);
                oModel.setProperty("/date", new Date(aFinancialDataBHeaders[0].create_date));
                oModel.setProperty("/view/pic", aFinancialDataBHeaders[0].pic);
                oModel.setProperty("/view/applicant", aFinancialDataBHeaders[0].applicant);
                oModel.setProperty("/view/company_code", aFinancialDataBHeaders[0].company_code);
                oModel.setProperty("/view/customer_code", aFinancialDataBHeaders[0].customer_code);
                oModel.setProperty("/view/customer_name", aFinancialDataBHeaders[0].customer_name);
                oModel.setProperty("/view/attribute", aFinancialDataBHeaders[0].attribute);
                oModel.setProperty("/view/year", aFinancialDataBHeaders[0].year);
                oModel.setProperty("/view/quarter", aFinancialDataBHeaders[0].quarter);
                oModel.setProperty("/view/year2", aFinancialDataBHeaders[0].year2);
                oModel.setProperty("/view/month", aFinancialDataBHeaders[0].month);
                oModel.setProperty("/view/currency", aFinancialDataBHeaders[0].currency);
                oModel.setProperty("/view/unit", aFinancialDataBHeaders[0].unit);

                var sfilterd = "?$filter=(application_no eq '" + sObjectId + "')";
                var aFinancialDataItems = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/FinancialDataService/FinancialDataItems") + sfilterd,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aFinancialDataItems = data.value; }
                });

                var aSmallClasses = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/SmallClasses"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aSmallClasses = data.value; }
                });
                /*        var sAmount = "";
                       var aDetails = [];
                       for (var i = 0; i < aSmallClasses.length; i++) {
                           sAmount = "";
                           for (var _i = 0; _i < aFinancialDataItems.length; _i++) {
                               if (aFinancialDataItems[_i].small_class_id == aSmallClasses[i].id) {
                                   sAmount = aFinancialDataItems[_i].amount;
                                   break;
                               }
                           }
       
                           var sDetails = {
                               id: aSmallClasses[i].id,
                               description_en: aSmallClasses[i].description_en,
                               description_zf: aSmallClasses[i].description_zf,
                               convert_whole_year: aSmallClasses[i].convert_whole_year,
                               ordering: aSmallClasses[i].ordering,
                               active: aSmallClasses[i].active,
                               amount: sAmount
                           }
                           aDetails.push(sDetails);
                       }
                       this.getView().getModel().setProperty("/details", aDetails); */

         
                var aDetails_copy = [];
                var iCeil = Math.ceil(aSmallClasses.length / 3);
                this.getView().getModel().setProperty("/details_count", iCeil);

                var amount,
                    id_copy1,
                    description_en_copy1,
                    description_zf_copy1,
                    convert_whole_year_copy1,
                    ordering_copy1,
                    active_copy1,
                    amount_copy1,
                    visible_copy1,

                    id_copy2,
                    description_en_copy2,
                    description_zf_copy2,
                    convert_whole_year_copy2,
                    ordering_copy2,
                    active_copy2,
                    amount_copy2,
                    visible_copy2;

                for (var i = 0; i < iCeil; i++) {

                    amount = "";
                    amount_copy1 = "";
                    amount_copy2 = "";
                    for (var _i = 0; _i < aFinancialDataItems.length; _i++) {
                        if (aFinancialDataItems[_i].small_class_id == aSmallClasses[i].id) {
                            amount = aFinancialDataItems[_i].amount;
                        }
                        if (aSmallClasses[i + iCeil] !== undefined) {
                            if (aFinancialDataItems[_i].small_class_id == aSmallClasses[i + iCeil].id) {
                                amount_copy1 = aFinancialDataItems[_i].amount;
                            }
                        }
                        if (aSmallClasses[i + iCeil + iCeil] !== undefined) {
                            if (aFinancialDataItems[_i].small_class_id == aSmallClasses[i + iCeil + iCeil].id) {
                                amount_copy2 = aFinancialDataItems[_i].amount;
                            }
                        }
                    }

                    if (aSmallClasses[i + iCeil] === undefined) {
                        id_copy1 = null;
                        description_en_copy1 = null;
                        description_zf_copy1 = null;
                        convert_whole_year_copy1 = null;
                        ordering_copy1 = null;
                        active_copy1 = null;
                        visible_copy1 = false;
                    } else {
                        id_copy1 = aSmallClasses[i + iCeil].id;
                        description_en_copy1 = aSmallClasses[i + iCeil].description_en;
                        description_zf_copy1 = aSmallClasses[i + iCeil].description_zf;
                        convert_whole_year_copy1 = aSmallClasses[i + iCeil].convert_whole_year;
                        ordering_copy1 = aSmallClasses[i + iCeil].ordering;
                        active_copy1 = aSmallClasses[i + iCeil].active;
                        visible_copy1 = true;
                    }

                    if (aSmallClasses[i + iCeil + iCeil] === undefined) {
                        id_copy2 = null;
                        description_en_copy2 = null;
                        description_zf_copy2 = null;
                        convert_whole_year_copy2 = null;
                        ordering_copy2 = null;
                        active_copy2 = null;
                        visible_copy2 = false;
                    } else {
                        id_copy2 = aSmallClasses[i + iCeil + iCeil].id;
                        description_en_copy2 = aSmallClasses[i + iCeil + iCeil].description_en;
                        description_zf_copy2 = aSmallClasses[i + iCeil + iCeil].description_zf;
                        convert_whole_year_copy2 = aSmallClasses[i + iCeil + iCeil].convert_whole_year;
                        ordering_copy2 = aSmallClasses[i + iCeil + iCeil].ordering;
                        active_copy2 = aSmallClasses[i + iCeil + iCeil].active;
                        visible_copy2 = true;
                    }

                    var sDetails_copy = {
                        id: aSmallClasses[i].id,
                        description_en: aSmallClasses[i].description_en,
                        description_zf: aSmallClasses[i].description_zf,
                        convert_whole_year: aSmallClasses[i].convert_whole_year,
                        ordering: aSmallClasses[i].ordering,
                        active: aSmallClasses[i].active,
                        amount: amount,
                        visible: true,
                        id_copy1: id_copy1,
                        description_en_copy1: description_en_copy1,
                        description_zf_copy1: description_zf_copy1,
                        convert_whole_year_copy1: convert_whole_year_copy1,
                        ordering_copy1: ordering_copy1,
                        active_copy1: active_copy1,
                        amount_copy1: amount_copy1,
                        visible_copy1: visible_copy1,
                        id_copy2: id_copy2,
                        description_en_copy2: description_en_copy2,
                        description_zf_copy2: description_zf_copy2,
                        convert_whole_year_copy2: convert_whole_year_copy2,
                        ordering_copy2: ordering_copy2,
                        active_copy2: active_copy2,
                        amount_copy2: amount_copy2,
                        visible_copy2: visible_copy2,
                    }

                    aDetails_copy.push(sDetails_copy);
                }

                this.getView().getModel().setProperty("/details_copy", aDetails_copy);
              
                var aFilter = [];
                aFilter.push(new sap.ui.model.Filter("application_no", sap.ui.model.FilterOperator.EQ, this.getView().getModel().getProperty("/view/application_no")));
                var oTableHeaders = this.getView().byId("idTableFinancialDataBHeaders");
                oTableHeaders.getBinding("items").filter(aFilter);

                var oTableItems = this.getView().byId("idTableFinancialDataItems");
                oTableItems.getBinding("items").filter(aFilter);

            },
            onChangeAmount: function (oEvent) {
                var input = oEvent.getSource().getValue();
                var aInput = input.split(".");
                if (aInput[1]) {
                    var NewInput = aInput[0].substr(0, 22) + "." + aInput[1].substr(0, 2);
                    oEvent.getSource().setValue(NewInput);
                }

            },
            _onInitJson: function () {
                this.getView().setModel(new sap.ui.model.json.JSONModel({
                    view: {
                        application_no: null,
                        create_date: null,
                        pic: null,
                        applicant: null,
                        company_code: null,
                        customer_code: null,
                        customer_name: null,
                        attribute: null,
                        year: null,
                        quarter: null,
                        year2: null,
                        month: null,
                        currency: null,
                        unit: null
                    },
                    details: [],
                    details_copy: [],
                    details_count: 0,
                    date: new Date(),
                    vaildinput: null
                }));
            },

            _onSaveEvent: function () {
                this.getView().setBusy(true);
                this._onSubmitVaild();
                this._onSetDate();
                if ((this.getView().getModel().getProperty("/vaildinput") == true)) {
                    var oTableHeaders = this.getView().byId("idTableFinancialDataBHeaders");
                    oTableHeaders.setSelectedItem(oTableHeaders.getItems()[0]);
                    var aContexts = oTableHeaders.getSelectedContexts();
                    if (aContexts[0].getObject().application_no == this.getView().getModel().getProperty("/view/application_no")) {
                        aContexts[0].setProperty("create_date", this.getView().getModel().getProperty("/view/create_date"));
                        aContexts[0].setProperty("pic", this.getView().getModel().getProperty("/view/pic"));
                        aContexts[0].setProperty("applicant", this.getView().getModel().getProperty("/view/applicant"));
                        aContexts[0].setProperty("company_code", this.getView().getModel().getProperty("/view/company_code"));
                        aContexts[0].setProperty("customer_code", this.getView().getModel().getProperty("/view/customer_code"));
                        aContexts[0].setProperty("customer_name", this.getView().getModel().getProperty("/view/customer_name"));
                        aContexts[0].setProperty("attribute", this.getView().getModel().getProperty("/view/attribute"));
                        aContexts[0].setProperty("year", this.getView().getModel().getProperty("/view/year"));
                        aContexts[0].setProperty("quarter", this.getView().getModel().getProperty("/view/quarter"));
                        aContexts[0].setProperty("year2", this.getView().getModel().getProperty("/view/year2"));
                        aContexts[0].setProperty("month", this.getView().getModel().getProperty("/view/month"));
                        aContexts[0].setProperty("currency", this.getView().getModel().getProperty("/view/currency"));
                        aContexts[0].setProperty("unit", this.getView().getModel().getProperty("/view/unit"));
                    }

                    /*                  var oTableItems = this.getView().byId("idTableFinancialDataItems");
                                     oTableItems.selectAll();
                                     var aDetails = this.getView().getModel().getProperty("/details");
                                     for (var i = 0; i < aDetails.length; i++) {
                                         if (!aDetails[i].amount) {
                                             var sPath = "/details/" + i + "/amount";
                                             this.getView().getModel().setProperty(sPath, 0);
                                         }
                                     }
                                     var aContexts = oTableItems.getSelectedContexts();
                                     for (var i = 0; i < aDetails.length; i++) {
                                         var sNewFlag = true;
                                         for (var _i = 0; _i < aContexts.length; ++_i) {
                                             var aContext = aContexts[_i];
                                             if ((aContext.getObject().application_no == this.getView().getModel().getProperty("/view/application_no")) &&
                                                 (aContext.getObject().small_class_id == aDetails[i].id)) {
                                                 aContext.setProperty("amount", aDetails[i].amount.toString());
                                                 sNewFlag = false;
                                                 break;
                                             }
                                         }
                                         if (sNewFlag == true) {
                                             oTableItems.getBinding("items").create({
                                                 application_no: this.getView().getModel().getProperty("/view/application_no"),
                                                 small_class_id: aDetails[i].id,
                                                 amount: aDetails[i].amount.toString()
                                             });
                                         }
                                     } */

                    var oTableItems = this.getView().byId("idTableFinancialDataItems");
                    oTableItems.selectAll();
                    var aDetails_copy = [];
                    var sDetails_copy = {};
                    var aDetails_temp = this.getView().getModel().getProperty("/details_copy");
                    for (var a = 0; a < 3; a++) {
                        for (var b = 0; b < aDetails_temp.length; b++) {
                            switch (a) {
                                case 0: {
                                    if (aDetails_temp[b].id) {
                                        sDetails_copy = {
                                            id: aDetails_temp[b].id,
                                            description_en: aDetails_temp[b].description_en,
                                            description_zf: aDetails_temp[b].description_zf,
                                            convert_whole_year: aDetails_temp[b].convert_whole_year,
                                            ordering: aDetails_temp[b].ordering,
                                            active: aDetails_temp[b].active,
                                            amount: aDetails_temp[b].amount,
                                        }
                                        aDetails_copy.push(sDetails_copy);
                                    }
                                    break;
                                }
                                case 1: {
                                    if (aDetails_temp[b].id_copy1) {
                                        sDetails_copy = {
                                            id: aDetails_temp[b].id_copy1,
                                            description_en: aDetails_temp[b].description_en_copy1,
                                            description_zf: aDetails_temp[b].description_zf_copy1,
                                            convert_whole_year: aDetails_temp[b].convert_whole_year_copy1,
                                            ordering: aDetails_temp[b].ordering_copy1,
                                            active: aDetails_temp[b].active_copy1,
                                            amount: aDetails_temp[b].amount_copy1,
                                        }
                                        aDetails_copy.push(sDetails_copy);
                                    }
                                    break;
                                }
                                case 2: {
                                    if (aDetails_temp[b].id_copy2) {
                                        sDetails_copy = {
                                            id: aDetails_temp[b].id_copy2,
                                            description_en: aDetails_temp[b].description_en_copy2,
                                            description_zf: aDetails_temp[b].description_zf_copy2,
                                            convert_whole_year: aDetails_temp[b].convert_whole_year_copy2,
                                            ordering: aDetails_temp[b].ordering_copy2,
                                            active: aDetails_temp[b].active_copy2,
                                            amount: aDetails_temp[b].amount_copy2,
                                        }
                                        aDetails_copy.push(sDetails_copy);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    for (var i = 0; i < aDetails_copy.length; i++) {
                        if (!aDetails_copy[i].amount) {
                            var sPath = "/details/" + i + "/amount";
                            this.getView().getModel().setProperty(sPath, 0);
                        }
                    }
                    var aContexts = oTableItems.getSelectedContexts();
                    for (var i = 0; i < aDetails_copy.length; i++) {
                        var sNewFlag = true;
                        for (var _i = 0; _i < aContexts.length; ++_i) {
                            var aContext = aContexts[_i];
                            if ((aContext.getObject().application_no == this.getView().getModel().getProperty("/view/application_no")) &&
                                (aContext.getObject().small_class_id == aDetails_copy[i].id)) {
                                aContext.setProperty("amount", aDetails_copy[i].amount.toString());
                                sNewFlag = false;
                                break;
                            }
                        }
                        if (sNewFlag == true) {
                            oTableItems.getBinding("items").create({
                                application_no: this.getView().getModel().getProperty("/view/application_no"),
                                small_class_id: aDetails_copy[i].id,
                                amount: aDetails_copy[i].amount.toString()
                            });
                        }
                    }
                    this._onSubmitBatch();
                }
                this.getView().setBusy(false);
            },
            _onSetDate: function () {
                this.getView().getModel().setProperty("/view/create_date", this.getView().byId("idDatePickercreate_date").getValue())
            },
            _onSubmitBatch: function () {
                var fnSuccess = function () {
                    MessageToast.show(this._getText("SuccessEditMessage"));
                }.bind(this);

                var fnError = function (oError) {
                    MessageBox.error(oError.message);
                }.bind(this);

                this.getView().getModel("FinancialData").submitBatch("UpdateGroup").then(fnSuccess, fnError);
            },
            _onSubmitVaild: function () {

                var aInputs = [
                    this.getView().byId("idInputunit"),
                    this.getView().byId("idInputquarter"),
                    this.getView().byId("idInputcurrency")
                ],
                    bValidationError = false;

                aInputs.forEach(function (oInput) {
                    bValidationError = this._validateInput(oInput) || bValidationError;
                    if (bValidationError == true) {
                        this._onSetVaildInput(false);
                    }
                }, this);
                if (!bValidationError) {

                } else {
                    MessageBox.alert(this._getText("message004"));

                }
            },
            _validateInput: function (oInput) {
                var sValueState = "None";
                var bValidationError = false;
                var oBinding = oInput.getBinding("value");

                if (oBinding.oValue == '' || oBinding.oValue == null) {
                    sValueState = "Error";
                    bValidationError = true;
                    this._onSetVaildInput(false);
                } else {
                    this._onSetVaildInput(true);
                }

                oInput.setValueState(sValueState);

                return bValidationError;
            },
            _onSetVaildInput: function (sInput) {
                this.getView().getModel().setProperty("/vaildinput", sInput);
            },
            _getText: function (sTextId, aArgs) {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sTextId, aArgs);
            },
            _onChange: function (oEvent) {
                var oInput = oEvent.getSource();
                this._validateInput(oInput);
            },
        });
    });
