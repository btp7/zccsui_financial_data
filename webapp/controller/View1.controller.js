sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, MessageBox, Filter, FilterOperator) {
        "use strict";

        return Controller.extend("zccsuifinancialdata.controller.View1", {
            onInit: function () {
                this._onInitJson();
                this._onReadData();
            },
            _onTESTButton: function () {
                var oTableHeaders = this.getView().byId("idTableFinancialDataBHeaders");
                oTableHeaders.getBinding("items").create({
                    application_no: this.getView().getModel().getProperty("/view/application_no"),
                    create_date: this.getView().getModel().getProperty("/view/create_date"),
                    pic: this.getView().getModel().getProperty("/view/pic"),
                    applicant: this.getView().getModel().getProperty("/view/applicant"),
                    company_code: this.getView().getModel().getProperty("/view/company_code"),
                    customer_code: this.getView().getModel().getProperty("/view/customer_code"),
                    customer_name: this.getView().getModel().getProperty("/view/customer_name"),
                    attribute: this.getView().getModel().getProperty("/view/attribute"),
                    year: this.getView().getModel().getProperty("/view/year"),
                    quarter: this.getView().getModel().getProperty("/view/quarter"),
                    year2: this.getView().getModel().getProperty("/view/year2"),
                    month: this.getView().getModel().getProperty("/view/month"),
                    currency: this.getView().getModel().getProperty("/view/currency"),
                    unit: this.getView().getModel().getProperty("/view/unit")
                });
                try {
                    var fnSuccess = function (o) {
                        MessageToast.show(this._getText("SuccessMessage"));
                    }.bind(this);

                    var fnError = function (oError) {
                        MessageBox.error(oError.message);
                    }.bind(this);

                    this.getView().getModel("FinancialData").submitBatch("UpdateGroup").then(fnSuccess, fnError);
                }
                catch (e) {
                }

            },
            _onInitJson: function () {

                this.getView().setModel(new sap.ui.model.json.JSONModel({
                    view: {
                        application_no: null,
                        create_date: null,
                        pic: null,
                        applicant: null,
                        company_code: null,
                        customer_code: null,
                        customer_name: null,
                        attribute: null,
                        year: null,
                        quarter: null,
                        year2: null,
                        month: null,
                        currency: null,
                        unit: null
                    },
                    message: null,
                    vaildinput: true,
                    date: new Date(),
                    details: [],
                    details_copy: [],
                    details_count: 0
                }));

            },
            _onChange: function (oEvent) {
                var oInput = oEvent.getSource();
                this._validateInput(oInput);
            },
            _validateInput: function (oInput) {
                var sValueState = "None";
                var bValidationError = false;
                var oBinding = oInput.getBinding("value");

                if (oBinding.oValue == '' || oBinding.oValue == null) {
                    sValueState = "Error";
                    bValidationError = true;
                    this._onSetVaildInput(false);
                } else {
                    this._onSetVaildInput(true);
                }

                oInput.setValueState(sValueState);

                return bValidationError;
            },
            _onSetVaildInput: function (sInput) {
                this.getView().getModel().setProperty("/vaildinput", sInput);
            },
            _onSubmitVaild: function () {

                var aInputs = [
                    this.getView().byId("idInputunit"),
                    this.getView().byId("idInputquarter"),
                    this.getView().byId("idInputcurrency")
                ],
                    bValidationError = false;

                aInputs.forEach(function (oInput) {
                    bValidationError = this._validateInput(oInput) || bValidationError;
                    if (bValidationError == true) {
                        this._onSetVaildInput(false);
                    }
                }, this);
                if (!bValidationError) {
                    /*    MessageToast.show("The input is validated. Your form has been submitted."); */
                } else {
                    MessageBox.alert(this._getText("message004"));

                }
            },
            _onQueryEvent: function (oEvent) {
                this._onSetDate();

                /*   this.byId("idLinkapplication_no").setHref("#" + this.getOwnerComponent().getRouter().getURL("RouteView1"));
                  debugger; */
                /*  if (!this._finhistoryDialog) {
                     this._finhistoryDialog = sap.ui.xmlfragment("zccsuifinancialdata.fragment.finhistory", this);
                     this.getView().addDependent(this._finhistoryDialog);
                 } */
                var aFilter = [];
                var oModel = this.getView().getModel();

                /*  if (oModel.getProperty("/view/application_no")) {
                     var oFilter = new Filter("application_no", FilterOperator.EQ, oModel.getProperty("/view/application_no"));
                     aFilter.push(oFilter);
                 } */
                if (oModel.getProperty("/view/create_date")) {
                    var oFilter = new Filter("create_date", FilterOperator.EQ, oModel.getProperty("/view/create_date"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/pic")) {
                    var oFilter = new Filter("pic", FilterOperator.EQ, oModel.getProperty("/view/pic"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/applicant")) {
                    var oFilter = new Filter("applicant", FilterOperator.EQ, oModel.getProperty("/view/applicant"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/company_code")) {
                    var oFilter = new Filter("company_code", FilterOperator.EQ, oModel.getProperty("/view/company_code"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/customer_code")) {
                    var oFilter = new Filter("customer_code", FilterOperator.EQ, oModel.getProperty("/view/customer_code"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/customer_name")) {
                    var oFilter = new Filter("customer_name", FilterOperator.EQ, oModel.getProperty("/view/customer_name"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/attribute")) {
                    var oFilter = new Filter("attribute", FilterOperator.EQ, oModel.getProperty("/view/attribute"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/year")) {
                    var oFilter = new Filter("year", FilterOperator.EQ, oModel.getProperty("/view/year"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/quarter")) {
                    var oFilter = new Filter("quarter", FilterOperator.EQ, oModel.getProperty("/view/quarter"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/year2")) {
                    var oFilter = new Filter("year2", FilterOperator.EQ, oModel.getProperty("/view/year2"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/month")) {
                    var oFilter = new Filter("month", FilterOperator.EQ, oModel.getProperty("/view/month"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/currency")) {
                    var oFilter = new Filter("currency", FilterOperator.EQ, oModel.getProperty("/view/currency"));
                    aFilter.push(oFilter);
                }

                if (oModel.getProperty("/view/unit")) {
                    var oFilter = new Filter("unit", FilterOperator.EQ, oModel.getProperty("/view/unit"));
                    aFilter.push(oFilter);
                }
                /* 
                                this._finhistoryDialog.getContent()[1].getBinding("rows").filter(aFilter); */

                /*    this.getView().byId("idTablefinhistory").getBinding("rows").filter(aFilter); */
                var oBinding = this.byId("idTablefinhistory").getBinding("rows");
                oBinding.refresh();
                oBinding.filter(aFilter);
                this.byId("idDialog").open();
            },

            _onSaveEvent: function () {
                this.getView().setBusy(true);
                this._onSubmitVaild();
                this._onSetDate();
                if (this.getView().getModel().getProperty("/vaildinput") == true) {
                    this._onMessageStatus(null);
                    this._onCheckInput();
                    if (!this.getView().getModel().getProperty("/message")) {
                        this._onSaveData();
                    } else {
                        MessageToast.show(this.getView().getModel().getProperty("/message"));
                    }
                }
                this.getView().setBusy(false);
            },
            _onReadData: function () {
                var aReturn = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/SmallClasses"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aReturn = data.value; }
                });
            /*     var aDetails = [];
                for (var i = 0; i < aReturn.length; i++) {
                    var sDetails = {
                        id: aReturn[i].id,
                        description_en: aReturn[i].description_en,
                        description_zf: aReturn[i].description_zf,
                        convert_whole_year: aReturn[i].convert_whole_year,
                        ordering: aReturn[i].ordering,
                        active: aReturn[i].active,
                        amount: null
                    }
                    aDetails.push(sDetails);
                }
                this.getView().getModel().setProperty("/details", aDetails); */

                var aDetails_copy = [];
                var iCeil = Math.ceil(aReturn.length / 3);
                this.getView().getModel().setProperty("/details_count", iCeil);

                var id_copy1,
                    description_en_copy1,
                    description_zf_copy1,
                    convert_whole_year_copy1,
                    ordering_copy1,
                    active_copy1,
                    amount_copy1,
                    visible_copy1,

                    id_copy2,
                    description_en_copy2,
                    description_zf_copy2,
                    convert_whole_year_copy2,
                    ordering_copy2,
                    active_copy2,
                    amount_copy2,
                    visible_copy2;

                for (var i = 0; i < iCeil; i++) {

                    if (aReturn[i + iCeil] === undefined) {
                        id_copy1 = null;
                        description_en_copy1 = null;
                        description_zf_copy1 = null;
                        convert_whole_year_copy1 = null;
                        ordering_copy1 = null;
                        active_copy1 = null;
                        visible_copy1 = false;
                    } else {
                        id_copy1 = aReturn[i + iCeil].id;
                        description_en_copy1 = aReturn[i + iCeil].description_en;
                        description_zf_copy1 = aReturn[i + iCeil].description_zf;
                        convert_whole_year_copy1 = aReturn[i + iCeil].convert_whole_year;
                        ordering_copy1 = aReturn[i + iCeil].ordering;
                        active_copy1 = aReturn[i + iCeil].active;
                        visible_copy1 = true;
                    }

                    if (aReturn[i + iCeil + iCeil] === undefined) {
                        id_copy2 = null;
                        description_en_copy2 = null;
                        description_zf_copy2 = null;
                        convert_whole_year_copy2 = null;
                        ordering_copy2 = null;
                        active_copy2 = null;
                        visible_copy2 = false;
                    } else {
                        id_copy2 = aReturn[i + iCeil + iCeil].id;
                        description_en_copy2 = aReturn[i + iCeil + iCeil].description_en;
                        description_zf_copy2 = aReturn[i + iCeil + iCeil].description_zf;
                        convert_whole_year_copy2 = aReturn[i + iCeil + iCeil].convert_whole_year;
                        ordering_copy2 = aReturn[i + iCeil + iCeil].ordering;
                        active_copy2 = aReturn[i + iCeil + iCeil].active;
                        visible_copy2 = true;
                    }

                    var sDetails_copy = {
                        id: aReturn[i].id,
                        description_en: aReturn[i].description_en,
                        description_zf: aReturn[i].description_zf,
                        convert_whole_year: aReturn[i].convert_whole_year,
                        ordering: aReturn[i].ordering,
                        active: aReturn[i].active,
                        amount: null,
                        visible: true,
                        id_copy1: id_copy1,
                        description_en_copy1: description_en_copy1,
                        description_zf_copy1: description_zf_copy1,
                        convert_whole_year_copy1: convert_whole_year_copy1,
                        ordering_copy1: ordering_copy1,
                        active_copy1: active_copy1,
                        amount_copy1: null,
                        visible_copy1: visible_copy1,
                        id_copy2: id_copy2,
                        description_en_copy2: description_en_copy2,
                        description_zf_copy2: description_zf_copy2,
                        convert_whole_year_copy2: convert_whole_year_copy2,
                        ordering_copy2: ordering_copy2,
                        active_copy2: active_copy2,
                        amount_copy2: null,
                        visible_copy2: visible_copy2,
                    }

                    aDetails_copy.push(sDetails_copy);
                }

                this.getView().getModel().setProperty("/details_copy", aDetails_copy);

            },
            _onMessageStatus: function (messagestatus) {
                this.getView().getModel().setProperty("/message", messagestatus);
            },
            _onCheckInput: function () {
                var aFinancialDataBHeaders = {};
                var sCreate_date = "",
                    sPIC = "",
                    sApplicant = "",
                    sCompany_code = "",
                    sCustomer_code = "",
                    sCustomer_name = "",
                    sAttribute = "",
                    sYear = "",
                    sQuarter = "",
                    sYear2 = "",
                    sMonth = "",
                    sCurrency = "",
                    sUnit = "";

                sCreate_date = this.getView().getModel().getProperty("/view/create_date");

                if (this.getView().getModel().getProperty("/view/pic") == null) {
                    sPIC = this.getView().getModel().getProperty("/view/pic");
                } else {
                    sPIC = "'" + this.getView().getModel().getProperty("/view/pic") + "'";
                }

                if (this.getView().getModel().getProperty("/view/applicant") == null) {
                    sApplicant = this.getView().getModel().getProperty("/view/applicant");
                } else {
                    sApplicant = "'" + this.getView().getModel().getProperty("/view/applicant") + "'";
                }

                if (this.getView().getModel().getProperty("/view/company_code") == null) {
                    sCompany_code = this.getView().getModel().getProperty("/view/company_code");
                } else {
                    sCompany_code = "'" + this.getView().getModel().getProperty("/view/company_code") + "'";
                }

                if (this.getView().getModel().getProperty("/view/customer_code") == null) {
                    sCustomer_code = this.getView().getModel().getProperty("/view/customer_code");
                } else {
                    sCustomer_code = "'" + this.getView().getModel().getProperty("/view/customer_code") + "'";
                }

                if (this.getView().getModel().getProperty("/view/customer_name") == null) {
                    sCustomer_name = this.getView().getModel().getProperty("/view/customer_name");
                } else {
                    sCustomer_name = "'" + this.getView().getModel().getProperty("/view/customer_name") + "'";
                }

                if (this.getView().getModel().getProperty("/view/attribute") == null) {
                    sAttribute = this.getView().getModel().getProperty("/view/attribute");
                } else {
                    sAttribute = "'" + this.getView().getModel().getProperty("/view/attribute") + "'";
                }

                if (this.getView().getModel().getProperty("/view/year") == null) {
                    sYear = this.getView().getModel().getProperty("/view/year");
                } else {
                    sYear = "'" + this.getView().getModel().getProperty("/view/year") + "'";
                }

                if (this.getView().getModel().getProperty("/view/quarter") == null) {
                    sQuarter = this.getView().getModel().getProperty("/view/quarter");
                } else {
                    sQuarter = "'" + this.getView().getModel().getProperty("/view/quarter") + "'";
                }

                if (this.getView().getModel().getProperty("/view/year2") == null) {
                    sYear2 = this.getView().getModel().getProperty("/view/year2");
                } else {
                    sYear2 = "'" + this.getView().getModel().getProperty("/view/year2") + "'";
                }

                if (this.getView().getModel().getProperty("/view/month") == null) {
                    sMonth = this.getView().getModel().getProperty("/view/month");
                } else {
                    sMonth = "'" + this.getView().getModel().getProperty("/view/month") + "'";
                }

                if (this.getView().getModel().getProperty("/view/currency") == null) {
                    sCurrency = this.getView().getModel().getProperty("/view/currency");
                } else {
                    sCurrency = "'" + this.getView().getModel().getProperty("/view/currency") + "'";
                }

                if (this.getView().getModel().getProperty("/view/unit") == null) {
                    sUnit = this.getView().getModel().getProperty("/view/unit");
                } else {
                    sUnit = "'" + this.getView().getModel().getProperty("/view/unit") + "'";
                }

                var sfilter = "?$filter=(create_date eq " + sCreate_date +
                    ") and (pic eq " + sPIC +
                    ") and (applicant eq " + sApplicant +
                    ") and (company_code eq " + sCompany_code +
                    ") and (customer_code eq " + sCustomer_code +
                    ") and (customer_name eq " + sCustomer_name +
                    ") and (attribute eq " + sAttribute +
                    ") and (year eq " + sYear +
                    ") and (quarter eq " + sQuarter +
                    ") and (year2 eq " + sYear2 +
                    ") and (month eq " + sMonth +
                    ") and (currency eq " + sCurrency +
                    ") and (unit eq " + sUnit +
                    ")";

                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/FinancialDataService/FinancialDataBHeaders") + sfilter,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aFinancialDataBHeaders = data.value; }
                });

                var sMessage = null;
                if (aFinancialDataBHeaders.length > 0) {
                    this._onSetApplicationNumber(aFinancialDataBHeaders[0].application_no);
                    sMessage = this._getText("message001") + " " + aFinancialDataBHeaders[0].application_no + " " + this._getText("message002");
                } else {
                    this._onSetApplicationNumber(globalThis.crypto.randomUUID().substr(0, 15));
                }

                if (sMessage) {
                    this._onMessageStatus(sMessage);
                }
            },
            _onSetDate: function () {
                if (this.getView().byId("idDatePickercreate_date").getValue() == '') {
                    this.getView().getModel().setProperty("/view/create_date", null);
                } else {
                    this.getView().getModel().setProperty("/view/create_date", this.getView().byId("idDatePickercreate_date").getValue())
                }

            },
            _onSetApplicationNumber: function (sAppnumber) {
                this.getView().getModel().setProperty("/view/application_no", sAppnumber);
            },
            _getText: function (sTextId, aArgs) {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sTextId, aArgs);
            },
            _onSaveData: function () {
                var oTableHeaders = this.getView().byId("idTableFinancialDataBHeaders");
                oTableHeaders.getBinding("items").create({
                    application_no: this.getView().getModel().getProperty("/view/application_no"),
                    create_date: this.getView().getModel().getProperty("/view/create_date"),
                    pic: this.getView().getModel().getProperty("/view/pic"),
                    applicant: this.getView().getModel().getProperty("/view/applicant"),
                    company_code: this.getView().getModel().getProperty("/view/company_code"),
                    customer_code: this.getView().getModel().getProperty("/view/customer_code"),
                    customer_name: this.getView().getModel().getProperty("/view/customer_name"),
                    attribute: this.getView().getModel().getProperty("/view/attribute"),
                    year: this.getView().getModel().getProperty("/view/year"),
                    quarter: this.getView().getModel().getProperty("/view/quarter"),
                    year2: this.getView().getModel().getProperty("/view/year2"),
                    month: this.getView().getModel().getProperty("/view/month"),
                    currency: this.getView().getModel().getProperty("/view/currency"),
                    unit: this.getView().getModel().getProperty("/view/unit")
                });

                /*  var oTableItems = this.getView().byId("idTableFinancialDataItems");
                 var aDetails = this.getView().getModel().getProperty("/details");
                 for (var i = 0; i < aDetails.length; i++) {
                     var sAmount = null;
                     if (!aDetails[i].amount) {
                         var sPath = "/details/" + i + "/amount";
                         this.getView().getModel().setProperty(sPath, 0);
                         sAmount = 0;
                     } else {
                         sAmount = aDetails[i].amount;
                     }
                     oTableItems.getBinding("items").create({
                         application_no: this.getView().getModel().getProperty("/view/application_no"),
                         small_class_id: aDetails[i].id,
                         amount: sAmount.toString()
 
                     });
                 } */

                 
                var oTableItems = this.getView().byId("idTableFinancialDataItems");
                var aDetails_copy = [];
                var sDetails_copy = {};
                var aDetails_temp = this.getView().getModel().getProperty("/details_copy");
                for (var a = 0; a < 3; a++) {
                    for (var b = 0; b < aDetails_temp.length; b++) {
                        switch (a) {
                            case 0: {
                                if (aDetails_temp[b].id) {
                                    sDetails_copy = {
                                        id: aDetails_temp[b].id,
                                        description_en: aDetails_temp[b].description_en,
                                        description_zf: aDetails_temp[b].description_zf,
                                        convert_whole_year: aDetails_temp[b].convert_whole_year,
                                        ordering: aDetails_temp[b].ordering,
                                        active: aDetails_temp[b].active,
                                        amount: aDetails_temp[b].amount,
                                    }
                                    aDetails_copy.push(sDetails_copy);
                                }
                                break;
                            }
                            case 1: {
                                if (aDetails_temp[b].id_copy1) {
                                    sDetails_copy = {
                                        id: aDetails_temp[b].id_copy1,
                                        description_en: aDetails_temp[b].description_en_copy1,
                                        description_zf: aDetails_temp[b].description_zf_copy1,
                                        convert_whole_year: aDetails_temp[b].convert_whole_year_copy1,
                                        ordering: aDetails_temp[b].ordering_copy1,
                                        active: aDetails_temp[b].active_copy1,
                                        amount: aDetails_temp[b].amount_copy1,
                                    }
                                    aDetails_copy.push(sDetails_copy);
                                }
                                break;
                            }
                            case 2: {
                                if (aDetails_temp[b].id_copy2) {
                                    sDetails_copy = {
                                        id: aDetails_temp[b].id_copy2,
                                        description_en: aDetails_temp[b].description_en_copy2,
                                        description_zf: aDetails_temp[b].description_zf_copy2,
                                        convert_whole_year: aDetails_temp[b].convert_whole_year_copy2,
                                        ordering: aDetails_temp[b].ordering_copy2,
                                        active: aDetails_temp[b].active_copy2,
                                        amount: aDetails_temp[b].amount_copy2,
                                    }
                                    aDetails_copy.push(sDetails_copy);
                                }
                                break;
                            }
                        }
                    }

                }

                for (var i = 0; i < aDetails_copy.length; i++) {
                    var sAmount = null;
                    if (!aDetails_copy[i].amount) {
                        var sPath = "/details/" + i + "/amount";
                        this.getView().getModel().setProperty(sPath, 0);
                        sAmount = 0;
                    } else {
                        sAmount = aDetails_copy[i].amount;
                    }
                    oTableItems.getBinding("items").create({
                        application_no: this.getView().getModel().getProperty("/view/application_no"),
                        small_class_id: aDetails_copy[i].id,
                        amount: sAmount.toString()

                    });
                }

                this._onSubmitBatch();
            },
            _onSubmitBatch: function () {
                var fnSuccess = function () {
                    MessageToast.show(this._getText("SuccessMessage"));
                }.bind(this);

                var fnError = function (oError) {
                    MessageBox.error(oError.message);
                }.bind(this);

                this.getView().getModel("FinancialData").submitBatch("UpdateGroup").then(fnSuccess, fnError);
            },
            _onFinhisClose: function () {
                this.byId("idDialog").close();
            },
            _onLinkPress: function (oEvent) {
                var objectId = oEvent.getSource().getText();
                var oRouter = this.getOwnerComponent().getRouter();
                oRouter.navTo("RouteView2", { objectId: objectId });
            },
            _onRefreshEvent: function () {
                var oModel = this.getView().getModel();
                oModel.setProperty("/view/application_no", null);
                oModel.setProperty("/view/create_date", null);
                oModel.setProperty("/date", null);
                oModel.setProperty("/view/pic", null);
                oModel.setProperty("/view/applicant", null);
                oModel.setProperty("/view/company_code", null);
                oModel.setProperty("/view/customer_code", null);
                oModel.setProperty("/view/customer_name", null);
                oModel.setProperty("/view/attribute", null);
                oModel.setProperty("/view/year", null);
                oModel.setProperty("/view/quarter", null);
                oModel.setProperty("/view/year2", null);
                oModel.setProperty("/view/month", null);
                oModel.setProperty("/view/currency", null);
                oModel.setProperty("/view/unit", null);

                this._onReadData();
            },

            onInputLiveChange: function (oEvent) {
                var oControl = oEvent.getSource();
                this.validateFloatInput(oControl);
            },
            onInputChange: function (oEvent) {
                var oControl = oEvent.getSource();
                this.validateFloatInput(oControl);
            },
            validateFloatInput: function (oControl) {
                var oBinding = oControl.getBinding("value");
                var oValue = oControl.getValue();
                try {
                    var oParsedValue = oBinding.getType().parseValue(oValue, oBinding.sInternalType); // throw error if cannot parse value
                    if (oParsedValue) {
                        oControl.setValueState(sap.ui.core.ValueState.None);
                    } else {
                        oControl.setValueState(sap.ui.core.ValueState.Error);
                    }
                } catch (ex) {
                    oControl.setValueState(sap.ui.core.ValueState.Error);
                }
            },
            onChangeAmount: function (oEvent) {
                var input = oEvent.getSource().getValue();
                var aInput = input.split(".");
                if (aInput[1]) {
                    var NewInput = aInput[0].substr(0, 22) + "." + aInput[1].substr(0, 2);
                    oEvent.getSource().setValue(NewInput);
                }

            },

        });
    });
