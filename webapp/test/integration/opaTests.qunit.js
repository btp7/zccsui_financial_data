/* global QUnit */

sap.ui.require(["zccsuifinancialdata/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
